%title: Infra Cloud Infomaniak
%author: xavki


██╗  ██╗███████╗██╗   ██╗ ██████╗██╗      ██████╗  █████╗ ██╗  ██╗
██║ ██╔╝██╔════╝╚██╗ ██╔╝██╔════╝██║     ██╔═══██╗██╔══██╗██║ ██╔╝
█████╔╝ █████╗   ╚████╔╝ ██║     ██║     ██║   ██║███████║█████╔╝ 
██╔═██╗ ██╔══╝    ╚██╔╝  ██║     ██║     ██║   ██║██╔══██║██╔═██╗ 
██║  ██╗███████╗   ██║   ╚██████╗███████╗╚██████╔╝██║  ██║██║  ██╗
╚═╝  ╚═╝╚══════╝   ╚═╝    ╚═════╝╚══════╝ ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝



-----------------------------------------------------------------------------------------------------------

# Keycloak : Cluster Management

<br>

Purpose : https://grafana.com/docs/grafana/latest/setup-grafana/configure-security/configure-authentication/keycloak/

-----------------------------------------------------------------------------------------------------------

# Keycloak : Cluster Management

<br>

1 - create a realm

2- create a client 


-----------------------------------------------------------------------------------------------------------

# Keycloak : Cluster Management

<br>

Create a new role

Some variables

```
keycloak_admin_user: xavkadmin
keycloak_admin_user_password: password
keycloak_url: https://k.xavki.fr/auth/
keycloak_realm: infrastructure
```

in all file

-----------------------------------------------------------------------------------------------------------

# Keycloak : Cluster Management

<br>

In all file

```
  all_keycloak_realm: infrastructure
  all_keycloak_client_id: grafana
  all_keycloak_client_secret: mysecret
  all_keycloak_client_url: g.xavki.fr
  all_keycloak_url: https://k.xavki.fr
```

-----------------------------------------------------------------------------------------------------------

# Keycloak : Cluster Management

<br>

Create the realm loop

```
- name: Create Keycloak Realms
  become: false
  local_action:
    module: community.general.keycloak_realm  
    auth_client_id: admin-cli
    auth_keycloak_url: "{{ keycloak_url }}"
    auth_realm: master
    auth_username: "{{ keycloak_admin_user }}"
    auth_password: "{{ keycloak_admin_user_password }}"
    validate_certs: false
    state: present
    id: "{{ item }}"
    realm: "{{ item }}"
    enabled: true
  run_once: true
  loop: "{{ keycloak_realms  }}"
```

-----------------------------------------------------------------------------------------------------------

# Keycloak : Cluster Management

<br>

Create client loop and users loop

```
- name: manage keycloak clients
  include_tasks: client_management.yml
  loop: "{{ keycloak_clients.keys() | list }}"
```

-----------------------------------------------------------------------------------------------------------

# Keycloak : Cluster Management

<br>

Install a recent version of collection community.general

```
https://galaxy.ansible.com/ui/repo/published/community/general/
```

Warn : check your ansible version

-----------------------------------------------------------------------------------------------------------

# Keycloak : Cluster Management

<br>

Create a realm

```
- name: Create Keycloak Realms
  become: false
  local_action:
    module: community.general.keycloak_realm  
    auth_client_id: admin-cli
    auth_keycloak_url: "{{ keycloak_url }}"
    auth_realm: master
    auth_username: "{{ keycloak_admin_user }}"
    auth_password: "{{ keycloak_admin_user_password }}"
    validate_certs: false
    state: present
    id: "{{ item }}"
    realm: "{{ item }}"
    enabled: true
  loop: "{{ keycloak_realms }}"
  run_once: true
```

-----------------------------------------------------------------------------------------------------------

# Keycloak : Cluster Management

<br>

Create clients

```
- name: Create Keycloak Clients
  become: false
  local_action:
    module: community.general.keycloak_client
    auth_client_id: admin-cli
    auth_keycloak_url: "{{ keycloak_url }}"
    auth_realm: master
    auth_username: "{{ keycloak_admin_user }}"
    auth_password: "{{ keycloak_admin_user_password }}"
    validate_certs: false
    state: present
    realm: "{{ keycloak_realm }}"
    name: '{{ item }}'
    client_id: '{{ keycloak_clients[item]["client_id"] }}'
    description: '{{ keycloak_clients[item]["description"] }}'
    enabled: true
    client_authenticator_type: client-secret
    secret: '{{ keycloak_clients[item]["client_secret"] }}'
    service_accounts_enabled: true
    standard_flow_enabled: true
    implicit_flow_enabled: false
    public_client: false
    default_client_scopes:
      - email
      - offline_access
      - profile
      - roles
    direct_access_grants_enabled: true
    redirect_uris: '{{ keycloak_clients[item]["redirect_uris"] }}'
    web_origins: '{{ keycloak_clients[item]["redirect_uris"] }}'
    root_url: '{{ keycloak_clients[item]["client_url"] }}'
    admin_url: '{{ keycloak_clients[item]["client_url"] }}'
    base_url: '{{ keycloak_clients[item]["client_url"] }}'
    protocol: openid-connect
  run_once: true
```

-----------------------------------------------------------------------------------------------------------

# Keycloak : Cluster Management

<br>

Our new playbook

```
- name: install our keycloak
  hosts: meta-app_keycloak
  roles:
    - keycloak_management
  vars:
    keycloak_admin_user: "{{ all_keycloak_admin_user }}"
    keycloak_admin_user_password: "{{ all_keycloak_admin_password }}"
    keycloak_url: "{{ all_keycloak_url }}"
    keycloak_realms:
      - "{{ all_keycloak_realm }}"
    keycloak_clients:
      grafana:
        realm: "{{ all_keycloak_realm }}"
        client_id: "{{ all_keycloak_client_id }}"
        client_secret: "{{ all_keycloak_client_secret }}" # define in all
        redirect_uris:
          - "https://{{ all_keycloak_client_url }}/*"
          - "http://{{ all_keycloak_client_url }}/*"
        client_url: "https://{{ all_keycloak_client_url }}" 
        description: "Client for grafana users"
        client_roles:
          - admin
          - editor
          - viewer
```

