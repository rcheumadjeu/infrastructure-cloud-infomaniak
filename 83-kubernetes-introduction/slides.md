%title: Infra Cloud Infomaniak
%author: xavki


██╗  ██╗██╗   ██╗██████╗ ███████╗██████╗ ███╗   ██╗███████╗████████╗███████╗███████╗
██║ ██╔╝██║   ██║██╔══██╗██╔════╝██╔══██╗████╗  ██║██╔════╝╚══██╔══╝██╔════╝██╔════╝
█████╔╝ ██║   ██║██████╔╝█████╗  ██████╔╝██╔██╗ ██║█████╗     ██║   █████╗  ███████╗
██╔═██╗ ██║   ██║██╔══██╗██╔══╝  ██╔══██╗██║╚██╗██║██╔══╝     ██║   ██╔══╝  ╚════██║
██║  ██╗╚██████╔╝██████╔╝███████╗██║  ██║██║ ╚████║███████╗   ██║   ███████╗███████║
╚═╝  ╚═╝ ╚═════╝ ╚═════╝ ╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝   ╚═╝   ╚══════╝╚══════╝




-----------------------------------------------------------------------------------------------------------

# Kubernetes : Introduction - what is it ??

<br>

Containers were pretty cool :

  but only a new package deliverable with many improvements

<br>

kubernetes, what is it ? 

  * a container orchestrator
    * at the beginning with docker
    * now : containerd, crio...

  * manage a cluster of containers

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Introduction - what is it ??

<br>

It came from Google : Borg (internal orchestrator)

  Great video : John Wilkes - Cluster management at Google with Borg
  https://www.youtube.com/watch?v=wy3L7XUq-g0

One of the major cloud service

<br>

Language : golang
Documentation : https://kubernetes.io/
Github : https://github.com/kubernetes/kubernetes

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Introduction - what is it ??

<br>

Why to use it ?

<br>

  * scaling : to handle some various worloads (increase/decrease instances number)
    for example : an ecommerce website

<br>

  * autohealing : in the past (or not), apps could need to be restart
    or need to remove an instance of a loadbalancer when an instance respond an error

<br>

  * distribution : to improve the instance distribution on all your infrastructure
    for example : to improve the density in DC or cloud

<br>

  * deployments : to deploy more easily with a better process (ci/cd...)

<br>

  * abstraction : to add an abstraction layer to not care about hardware and OS

<br>

  * and by the way : to improve observability (and tracing)

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Introduction - what is it ??

<br>

Kubernetes kind of nodes :

  * master nodes : manage the cluster, composed the control plane

  * worker nodes : to execute the workload

<br>

Kubernetes is composed of different services, specific to each kind of nodes

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Introduction - what is it ??

<br>

Some clients :

  * send requests over kubectl or code librairies

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Introduction - what is it ??

<br>

On master nodes :

  * APIServer : to handle requests and serve
      what you want to have and what you have
      secure reads/writes to the etcd database (cluster)
      ensure communication with client : certificates/tokens

<br>

  * Database : by default an etcd cluster
      to store manifests
      and state of our cluster

<br>

  * Controller manager : manage all kinds of controllers and controller loop
      Example : deployment controller, replicaset controller...

<br>

  * Scheduler : disctibute pods over the cluster
      based on filtering and scoring of nodes

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Introduction - what is it ??

<br>

On worker nodes :

  * Kubelet : an agent
      which manage locally what the scheduler wants to do
      check the state of the node

<br>

  * Kube-proxy :
      responsible of a part of the network communication
      allow service communications


-----------------------------------------------------------------------------------------------------------

# Kubernetes : Introduction - what is it ??

<br>

And :

  * Container Runtime :
      manage process as containers (cgroups & namespaces)
      example : containerd

<br>

  * Container Network Interface :
      ensure the network communication (ex : iptables or ipvs)
      many CNI (ex : kube-router)

<br>

  * Container Storage Interface :
      ensure the storage and provide storage class to provision volumes
      ex : openebs...