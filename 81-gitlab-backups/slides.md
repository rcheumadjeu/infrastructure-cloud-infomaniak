%title: Infra Cloud Infomaniak
%author: xavki


 ██████╗ ██╗████████╗██╗      █████╗ ██████╗ 
██╔════╝ ██║╚══██╔══╝██║     ██╔══██╗██╔══██╗
██║  ███╗██║   ██║   ██║     ███████║██████╔╝
██║   ██║██║   ██║   ██║     ██╔══██║██╔══██╗
╚██████╔╝██║   ██║   ███████╗██║  ██║██████╔╝
 ╚═════╝ ╚═╝   ╚═╝   ╚══════╝╚═╝  ╚═╝╚═════╝ 



-----------------------------------------------------------------------------------------------------------

# Gitlab : Install && Schedule backups

<br>

Add a bucket 

```
aws --endpoint-url=https://s3.pub1.infomaniak.cloud s3api create-bucket --bucket gitlab
```

Add default

```
gitlab_backup_s3_enabled: false
gitlab_backup_s3_access_key: ""
gitlab_backup_s3_secret_key: ""
gitlab_backup_s3_url: ""
gitlab_backup_s3_bucket: ""
gitlab_backup_cron_schedule: "0 1 * * *"
```

-----------------------------------------------------------------------------------------------------------

# Gitlab : Install && Schedule backups

<br>

Create a backup.j2 template

```
gitlab_rails['object_store']['enabled'] = true
gitlab_rails['backup_keep_time'] = 604800
gitlab_rails['backup_upload_connection'] = {
  'provider' => 'AWS',
  'region' => 'us-east-1',
  'aws_access_key_id' => '{{ gitlab_backup_s3_access_key }}',
  'aws_secret_access_key' => '{{ gitlab_backup_s3_secret_key }}',
  'endpoint'              => '{{ gitlab_backup_s3_url }}',
  'path_style' => true,
  'enable_signature_v4_streaming' => false,
  'use_iam_profile' => false
}
gitlab_rails['backup_upload_remote_directory'] = '{{ gitlab_backup_s3_bucket }}'
```

-----------------------------------------------------------------------------------------------------------

# Gitlab : Install && Schedule backups

<br>

Add a blockfile in a dedicated task file

```
- blockinfile:
    dest: /etc/gitlab/gitlab.rb
    block: "{{ lookup('template', 'backup.j2') }}"
    marker: "# {mark} ANSIBLE MANAGED BLOCK FOR Backups"
  notify: reconfigure_gitlab
```

-----------------------------------------------------------------------------------------------------------

# Gitlab : Install && Schedule backups

<br>

Add an include file with the new file

```
- name: include gitlab backups
  include_tasks: backups.yml
  when: gitlab_backup_s3_enabled
```

-----------------------------------------------------------------------------------------------------------

# Gitlab : Install && Schedule backups

<br>

Create a new cron

```
- name: add a cron for gitlab backups
  template:
    src: backup_gitlab.j2
    dest: /etc/cron.d/backup_gitlab
    owner: root
    group: root
    mode: 0750
```

-----------------------------------------------------------------------------------------------------------

# Gitlab : Install && Schedule backups

<br>

Create the cron template

```
## Edited by ansible
{{ gitlab_backup_cron_schedule }} root touch /var/lib/cron/start_gitlab_backup.txt && /usr/bin/gitlab-backup create && touch /var/lib/cron/end_gitlab_backup.txt
```