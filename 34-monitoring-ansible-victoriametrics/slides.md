%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - VictoriaMetrics & ansible

<br>

VictoriaMetrics

  * doc : https://docs.victoriametrics.com/

  * playlist on the xavki channel

  * really performant system : high cardinalities, high compression, small fingerprint

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - VictoriaMetrics & ansible

<br>

Step by step = smae as previous tools :)

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - VictoriaMetrics & ansible

<br>

Some variables

```
vmetrics_dir_disk_data: ""
vmetrics_version: 1.91.3
vmetrics_dir_data: "{{ vmetrics_dir_disk_data }}/var/lib/vmetrics"
vmetrics_listen: "0.0.0.0:8428"
vmetrics_month_retention: 12
```


-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - VictoriaMetrics & ansible

<br>

Some checks

```
- name: check if vmetrics exists
  stat:
    path: /usr/local/bin/victoria-metrics-prod
  register: __vmetrics_exists

- name: if vmetrics exists get version
  shell: "cat /etc/systemd/system/vmetrics.service | grep Version | sed s/'.*Version '//g"
  register: __get_vmetrics_version
  when: __vmetrics_exists.stat.exists == true
  changed_when: false
```


-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - VictoriaMetrics & ansible

<br>

Create user

```
- name: create user vmetrics
  user:   
    name: vmetrics
    system: yes
    shell: /sbin/nologin
    state: present
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - VictoriaMetrics & ansible

<br>

Create directories

```
- name: create vmetrics directory
  file:
    path: "{{ vmetrics_dir_data }}"
    state: directory
    owner: vmetrics
    group: vmetrics
    mode: 0755
    recurse: yes
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - VictoriaMetrics & ansible

<br>

Install binary

```
- name: download vmetrics
  unarchive:
    src: https://github.com/VictoriaMetrics/VictoriaMetrics/releases/download/v{{ vmetrics_version }}/victoria-metrics-linux-amd64-v{{ vmetrics_version }}.tar.gz
    dest: /usr/local/bin
    remote_src: yes
  when: __vmetrics_exists.stat.exists == False or not __get_vmetrics_version.stdout == vmetrics_version
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - VictoriaMetrics & ansible

<br>

Install systemd service

```
- name: vmetrics configuration file
  template:
    src: vmetrics.service.j2
    dest: /etc/systemd/system/vmetrics.service           
    mode: 0755
  notify: restart_vmetrics
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - VictoriaMetrics & ansible

<br>

Start and handler

```
- meta: flush_handlers

- name: start vmetrics
  systemd:
    name: vmetrics
    state: started
    enabled: yes
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - VictoriaMetrics & ansible

<br>

The handler

```
- name: restart_vmetrics
  systemd:
    name: vmetrics
    state: restarted
    enabled: yes
    daemon_reload: yes
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - VictoriaMetrics & ansible

<br>

The handler

```
[Unit]
Description=Description=VictoriaMetrics service Version {{ vmetrics_version }}
After=network.target

[Service]
Type=simple
LimitNOFILE=2097152
ExecStart=/usr/local/bin/victoria-metrics-prod \
       -storageDataPath={{ vmetrics_dir_data }} \
       -httpListenAddr={{ vmetrics_listen }} \
       -selfScrapeInterval=10s \
       -retentionPeriod={{ vmetrics_month_retention }}
# {{ vmetrics_month_retention }} months

SyslogIdentifier=vmetrics
Restart=always

[Install]
WantedBy=multi-user.target
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - VictoriaMetrics & ansible

<br>

Add the playbook

```
  roles:
    - vmetrics

      - { 
          name: "vmetrics",
          type: "http",
          target: "http://127.0.0.1:8428",
          interval: "10s",
          port: 8428
				}

```
