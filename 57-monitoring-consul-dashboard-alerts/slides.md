%title: Infra Cloud Infomaniak
%author: xavki


 ██████╗ ██████╗ ███╗   ██╗███████╗██╗   ██╗██╗         
██╔════╝██╔═══██╗████╗  ██║██╔════╝██║   ██║██║         
██║     ██║   ██║██╔██╗ ██║███████╗██║   ██║██║         
██║     ██║   ██║██║╚██╗██║╚════██║██║   ██║██║         
╚██████╗╚██████╔╝██║ ╚████║███████║╚██████╔╝███████╗    
 ╚═════╝ ╚═════╝ ╚═╝  ╚═══╝╚══════╝ ╚═════╝ ╚══════╝    


-----------------------------------------------------------------------------------------------------------                                       

# Activate Consul metrics : self metrics and exporter

<br>

## Service failed par node

```
sum by(service_name, node, dc) (consul_health_service_status{check!~"_service_maintenance.*",status!="passing"})
```

-----------------------------------------------------------------------------------------------------------                                       

# Activate Consul metrics : self metrics and exporter

<br>


Alert if equal 1

```
sum by(service_name, node, dc) (consul_health_service_status{status!="passing"}) == 1
```

-----------------------------------------------------------------------------------------------------------                                       

# Activate Consul metrics : self metrics and exporter

<br>

## Global service impact (ratio)

```
          (
          sum(consul_health_service_status{status="passing"}) by (service_name)
           - sum(consul_health_service_status{status="maintenance"}) by (service_name))
            / sum(consul_health_service_status) by (service_name) <1
```

-----------------------------------------------------------------------------------------------------------                                       

# Activate Consul metrics : self metrics and exporter

<br>


## Si prise en compte de la maintenance pas d'alerte) 
* Warn : number of lines


```
        sum by(service_name,dc) (
          
            ## which services are failed per node
          (
          	sum by(service_name, node, dc) (consul_health_service_status{check!~"_service_maintenance.*",status!="passing"})  ## not in maint and failed
          	
            # how many services in maintenance ?
          	- (
                sum by(service_name, node, dc) (consul_service_checks)
          	   	- sum by(service_name, node, dc) (consul_health_service_status{check!~"_service_maintenance.*"}) ## global - not maintenance
          	)
          )
          
            ## remove node in maintenance (count number of node in maintenance on previous node/service)

          - on(node) group_left() 

            sum by(node) (
                sum by(node) (consul_health_node_status)
                - sum by(node) (consul_health_node_status{check!~"_node_maintenance"}) ## global - not maintenance
            )
        )
            
        / sum by(service_name,dc) (consul_health_service_status{check!~"_service_maintenance.*"}) # ratio on global instances
```

