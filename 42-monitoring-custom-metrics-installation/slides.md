%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Custom Metrics - Backups consul


<br>

Example : consul backups on Object Storage


Project :

    * storage consul datas /data/var/lib/consul

    * in a container

    * with restic : deduplication, organization, retention...

    * ansible automation

    * dashboard & alerting

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Custom Metrics


<br>

Fix... set -eu !!! and || true

<br>

```
METRICS_DIR="/var/lib/node_exporter"
METRICS_FILE="${METRICS_DIR}/backups-consul.prom"
TOUCH_DIR="/var/lib/cron/"
```


-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Custom Metrics


<br>


```
timestamp_file(){
    timestamp_of_file=$(stat -c %Y $1)
    echo ${timestamp_of_file}
}
editMetric () {
    echo "# HELP $1 $2"
    echo "# TYPE $1 $3"
    echo "$1{$4=\"$5\"} $6"
}
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Custom Metrics


<br>

```
mkdir -p ${METRICS_DIR}
rm -f ${METRICS_FILE}
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Custom Metrics


<br>

```
for file in $(ls ${TOUCH_DIR}/*_*_*.txt);do 
  if [ -f "$file" ];then
    step=$(basename $file | awk -F "_" '{print $1}')
    techno=$(basename $file | awk -F "_" '{print $2}')
    action=$(basename $file | awk -F "_" '{print $3}' | tr -d ".txt")
    timestamp_file=$(timestamp_file "$file")
    editMetric "${action}_${step}_timestamp" "${step} of ${action} in epoch" "gauge" "${action}" "${techno}" ${timestamp_file} >> ${METRICS_FILE}
  fi
done
```


-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Custom Metrics


<br>

Dashboard