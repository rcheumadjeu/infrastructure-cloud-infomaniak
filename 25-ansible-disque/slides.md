%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Monter un disque sur un volume

<br>

Main task

```
- name: "Install disk - {{ item.disk }}"
  include_tasks: install_disk.yml
  with_items: "{{ volumes_disks }}"
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Monter un disque sur un volume

<br>

install disk task

```
- name: create ext4 File System
  filesystem:
    fstype: ext4
    dev: "{{ item.disk }}"
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Monter un disque sur un volume

<br>

install disk task

```
- name: ensure directory exists
  file:
    path: "{{ item.path }}"
    state: directory
    owner: "{{ item.owner | default('root') }}"
    group: "{{ item.owner | default('root') }}"
    mode: 0755
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Monter un disque sur un volume

<br>

install disk task

```
- name: check if the path already exists in fstab
  lineinfile:
    name: /etc/fstab
    line: "{{ item.path }}"
    state: present
  check_mode: yes
  register: __result_path
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Monter un disque sur un volume

<br>

install disk task

```
- name: Dispaly uuid & store in variable
  command: blkid -s UUID -o value {{ item.disk }}
  register: __uuid_disk
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Monter un disque sur un volume

<br>

install disk task

```
- name: Mount File System
  mount:
    path: "{{ item.path }}"
    src: "UUID={{ __uuid_disk.stdout }}"
    fstype: ext4
    opts: rw,nofail,noatime,auto
    state: mounted
  when: __result_path.changed
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Monter un disque sur un volume

<br>

Dedicated vars

```
    volumes_disks:
      - {disk: '/dev/sdb',path: '/data'}
```
