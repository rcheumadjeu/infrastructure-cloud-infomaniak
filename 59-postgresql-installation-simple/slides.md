%title: Infra Cloud Infomaniak
%author: xavki


██████╗  ██████╗ ███████╗████████╗ ██████╗ ██████╗ ███████╗███████╗ ██████╗ ██╗     
██╔══██╗██╔═══██╗██╔════╝╚══██╔══╝██╔════╝ ██╔══██╗██╔════╝██╔════╝██╔═══██╗██║     
██████╔╝██║   ██║███████╗   ██║   ██║  ███╗██████╔╝█████╗  ███████╗██║   ██║██║     
██╔═══╝ ██║   ██║╚════██║   ██║   ██║   ██║██╔══██╗██╔══╝  ╚════██║██║▄▄ ██║██║     
██║     ╚██████╔╝███████║   ██║   ╚██████╔╝██║  ██║███████╗███████║╚██████╔╝███████╗
╚═╝      ╚═════╝ ╚══════╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝╚══════╝╚══════╝ ╚══▀▀═╝ ╚══════╝



-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : installation & improvements

<br>

Some changes :

  * consul directory : consul + consul_services + backup_consul

  * base_server directory : base + dns-common

  * logging : move logrotate

  * openvpn directory : move client and server

  * create databases directory : sub directory postgresql


-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : installation & improvements

<br>

Need to do :

  * create two new instances with terraform

  * need to create a new role (only for a single postgresql)

  * add pgdg repository


Note : directories

  * 2 main directories : /var/lib/postgresql (PGDATA) + /etc/postgresql

  * main files :

      * postgresql.conf (or custom dir)

      * pg_hba.conf
  

 -----------------------------------------------------------------------------------------------------------                                       

# Postgresql : installation & improvements

<br>

A new instance

``` 
module "postgresql" {
  source                      = "../modules/instance"
  instance_count              = 2
  instance_name               = "postgresql"
  instance_key_pair           = "default_key"
  instance_security_groups    = ["all_internal"]
  instance_network_internal   = var.network_internal_dev
  instance_ssh_key = var.ssh_public_key_default_user
  instance_default_user = var.default_user
  instance_volumes_count = 1  
  metadatas                   = {
    environment          = "dev",
    app         = "postgresql"
  }
}
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : installation & improvements

<br>

Some variables

```
postgresql_simple_version_major: 16
postgresql_simple_version_detail: 16+257.pgdg120+1
postgresql_simple_repo_key: https://www.postgresql.org/media/keys/ACCC4CF8.asc
postgresql_simple_repo_url: http://apt.postgresql.org/pub/repos/apt/
postgresql_simple_listen: "*"
postgresql_simple_params: ""
postgresql_simple_data_dir: ""
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : installation & improvements

<br>

Add pgdg repository

```
- name: add gpg key
  ansible.builtin.apt_key:
    url: "{{ postgresql_simple_repo_key }}"

- name: add repository
  ansible.builtin.apt_repository:
    repo: "deb {{ postgresql_simple_repo_url }} {{ ansible_distribution_release }}-pgdg main"
    state: present
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : installation & improvements

<br>

Install postgresql

```
- name: install postgresql and contrib
  ansible.builtin.apt:
    name: "postgresql={{ postgresql_simple_version_detail }},postgresql-contrib"
    state: present
    update_cache: true
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : installation & improvements

<br>

Change the data dir ?

```
- name: create directory for postgresql data
  file:
    path: "{{ postgresql_simple_data_dir }}"
    state: directory
    mode: 0750
    owner: postgres
    group: postgres
  when: postgresql_simple_data_dir != ""
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : installation & improvements

<br>

Change main settings

```
- name: "postgresql.conf change listen address"
  lineinfile:
    path: "/etc/postgresql/{{ postgresql_simple_version_major }}/main/postgresql.conf"
    regex: "^listen_addresses"
    insertafter: '^#listen_addresses '
    line: "listen_addresses = '{{ postgresql_simple_listen }}'"
    state: present
  notify: restart_postgresql
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : installation & improvements

<br>

Cahnge the data dir

```
- name: "postgresql.conf change listen address"
  lineinfile:
    path: "/etc/postgresql/{{ postgresql_simple_version_major }}/main/postgresql.conf"
    regex: "^data_directory"
    insertafter: '^#data_directory '
    line: "data_directory = '{{ postgresql_simple_data_dir }}'"
    state: present
  when: postgresql_simple_data_dir != ""
  notify: reconfigure_data_dir
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : installation & improvements

<br>

Add custom file (if needed)

```
- name: "change postgres.conf to add include"
  lineinfile:
    path: "/etc/postgresql/{{ postgresql_simple_version_major }}/main/postgresql.conf"
    regex: "^include "
    insertafter: "^# Add settings for extensions here"
    line: "include 'postgresql.custom.conf'"
    state: present
  notify: restart_postgresql
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : installation & improvements

<br>

Push the custom file

```
- name: "add postgresql.custom.conf"
  template:
    src: postgresql.custom.conf.j2
    dest: /etc/postgresql/{{ postgresql_simple_version_major }}/main/postgresql.custom.conf
    mode: 0640
    owner: postgres
    group: postgres
  notify: restart_postgresql
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : installation & improvements

<br>

Set a new pg_hba

```
- name: add pg_hba.conf
  template:
    src: pg_hba.conf.j2
    dest: "/etc/postgresql/{{ postgresql_simple_version_major }}/main/pg_hba.conf"
    mode: 0640
    owner: postgres
    group: postgres
  notify: reload_postgresql
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : installation & improvements

<br>

Flush 

```
- meta: flush_handlers

- name: start postgresql
  service:
    name: postgresql
    state: started
    enabled: true
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : installation & improvements

<br>

Handlers

```
- name: restart_postgresql
  service:
    name: postgresql
    state: restarted
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : installation & improvements

<br>

Handlers

```
- name: reload_postgresql
  service:
    name: postgresql
    state: reloaded
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : installation & improvements

<br>

Handlers

```
- name: reconfigure_data_dir
  become: true
  become_user: postgres
  shell:
    cmd: |
      /usr/lib/postgresql/{{ postgresql_simple_version_major }}/bin/pg_ctl init -D {{ postgresql_simple_data_dir }}
  ignore_errors: true
  notify: restart_postgresql
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : installation & improvements

<br>

Template

```
{{ postgresql_simple_params }}
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : installation & improvements

<br>

Template

```
# DO NOT DISABLE!
# If you change this first entry you will need to make sure that the
# database superuser can access the database using some other method.
# Noninteractive access to all databases is required during automatic
# maintenance (custom daily cronjobs, replication, and similar tasks).
#
# Database administrative login by Unix domain socket
local   all             postgres                                peer

# TYPE  DATABASE        USER            ADDRESS                 METHOD

# "local" is for Unix domain socket connections only
local   all             all                                     peer

{% if postgresql_simple_pghba_entries %}
{% for entry in postgresql_simple_pghba_entries %}
{{ entry.host }}    {{ entry.database }}     {{ entry.user }}     {{ entry.address }}     {{ entry.method }}
{% endfor %}
{% endif %}
host    all             all             ::1/128                 scram-sha-256
host    all             all             127.0.0.1/32            scram-sha-256
```


-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : installation & improvements

<br>

Playbook

```
- name: install our postgresql cluster
  become: yes
  hosts: meta-app_postgresql
  roles:
    - volumes
    - databases/postgresql_simple
  vars:
    volumes_disks:
      - {disk: '/dev/sdb',path: '/data', owner: "root"}
    postgresql_simple_data_dir: /data/postgresql
```

