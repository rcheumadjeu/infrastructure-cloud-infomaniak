
module "test" {
  source                      = "../modules/instance"
  instance_count              = 3
  instance_name               = "test"
  instance_key_pair           = "default_key"
  instance_security_groups    = ["consul", "ssh-internal", "default","node_exporter"]
  instance_network_internal   = var.network_internal_dev
  instance_ssh_key = var.ssh_public_key_default_user
  instance_default_user = var.default_user
  metadatas                   = {
    environment          = "dev",
    app         = "test"
  }
}
      