%title: VictoriaMetrics
%author: xavki


█╗   ██╗██╗ ██████╗████████╗ ██████╗ ██████╗ ██╗ █████╗ 
██║   ██║██║██╔════╝╚══██╔══╝██╔═══██╗██╔══██╗██║██╔══██╗
██║   ██║██║██║        ██║   ██║   ██║██████╔╝██║███████║
╚██╗ ██╔╝██║██║        ██║   ██║   ██║██╔══██╗██║██╔══██║
 ╚████╔╝ ██║╚██████╗   ██║   ╚██████╔╝██║  ██║██║██║  ██║
  ╚═══╝  ╚═╝ ╚═════╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝╚═╝╚═╝  ╚═╝
███╗   ███╗███████╗████████╗██████╗ ██╗ ██████╗███████╗  
████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║██╔════╝██╔════╝  
██╔████╔██║█████╗     ██║   ██████╔╝██║██║     ███████╗  
██║╚██╔╝██║██╔══╝     ██║   ██╔══██╗██║██║     ╚════██║  
██║ ╚═╝ ██║███████╗   ██║   ██║  ██║██║╚██████╗███████║  
╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚═╝ ╚═════╝╚══════╝  
                                                         


-----------------------------------------------------------------------

# VICTORIA METRICS : Concepts & Definitions

<br>

TimeSeries

	* metric

	* key/value storage

  * specific compression models

	* the value change between scrape interval
		example :
			mymetric 102 1483228830.000
			mymetric 151 1483228850.000

  * comrpession example : delta / double-delta / bit / hash...

-----------------------------------------------------------------------

# VICTORIA METRICS : Concepts & Definitions

<br>

TimeSeries

	* Purposes :
			observe
			alert
			diagnostic

	* a standard format : openmetrics

		https://github.com/OpenObservability/OpenMetrics/

		metric{label=valeur_label,...}  value   timestamp_epoch

	* TSDB = agregation workloads = distributed column store


-----------------------------------------------------------------------

# VICTORIA METRICS : Concepts & Definitions

<br>

Labels

	* define specific metrics : for example an instance

	* allow us to filter or group by

	* example : load_average{instance="vm1"} 1.02 1483228830.000

	* 2 representations :

```
requests_total{path="/", code="200"} 
{__name__="requests_total", path="/", code="200"} 
```

-----------------------------------------------------------------------

# VICTORIA METRICS : Concepts & Definitions

<br>

Labels


	* timeseries = metric x label1 x label2 x labeln...

```
requests_total{path="/", code="404"} 100
requests_total{path="/", code="200"} 10
requests_total{path="/check", code="200"} 1001
```

Note : each line = raw sample


-----------------------------------------------------------------------

# VICTORIA METRICS : Concepts & Definitions

<br>

Cardinality

	* uniq timeseries (raw)

	* high cardinalities increase the fingerprint of the TSDB (ram,cpu)

```
requests_total{path="/", code="404"} 100
requests_total{path="/", code="200"} 10
requests_total{path="/check", code="200"} 1001
```

-----------------------------------------------------------------------

# VICTORIA METRICS : Concepts & Definitions

<br>

Kinds of metrics

	* gauge : numeric value, positive or negative
			ex : direct usage

	* counter : postitive value and aut-incremented
			ex : usage with rate

-----------------------------------------------------------------------

# VICTORIA METRICS : Concepts & Definitions

<br>

Kinds of metrics

	* histogram : store a distribution (in a bucket)

```
# TYPE http_request_duration_seconds histogram
http_request_duration_seconds_bucket{le="0.1"} 300
http_request_duration_seconds_bucket{le="0.5"} 400
http_request_duration_seconds_bucket{le="1.0"} 500
http_request_duration_seconds_bucket{le="5.0"} 700
http_request_duration_seconds_bucket{le="+Inf"} 1000
http_request_duration_seconds_sum 15000
http_request_duration_seconds_count 10000
```

-----------------------------------------------------------------------

# VICTORIA METRICS : Concepts & Definitions

<br>

Kinds of metrics

	* summary : same as histogram but only on quantiles

```
go_gc_duration_seconds{quantile="0"} 0
go_gc_duration_seconds{quantile="0.25"} 0
go_gc_duration_seconds{quantile="0.5"} 0
go_gc_duration_seconds{quantile="0.75"} 8.0696e-05
go_gc_duration_seconds{quantile="1"} 0.001222168
go_gc_duration_seconds_sum 0.015077078
go_gc_duration_seconds_count 83
```

-----------------------------------------------------------------------

# VICTORIA METRICS : Concepts & Definitions

<br>

Push / Pull Model / Scrape

	*	VM / Prometheus... pull models

	* scrape : gather metrics from all the infrastructure

	* scrape 
			a page in opentmetrics format
			a dedicated ip:port
			a default or specific route (/metrics)
			a scrape frequency (seconds/minutes/hours...)
			be careful : you can lose values between scrape !!

	* VM allows us to use push model

-----------------------------------------------------------------------

# VICTORIA METRICS : Concepts & Definitions

<br>

Exporters

	* expose metric pages in openmetric format

	* standard metrics (node-exporter) or dedicated to a tool
		ex : elasticsearch, mysql...

	* openmetric is a standard and many applications provide a metric page

-----------------------------------------------------------------------

# VICTORIA METRICS : Concepts & Definitions

<br>

Service Discovery

	* a service discovery provide a dynamic source of targets
		ex : consul, clouds, kubernetes, dns...

	* hot configuration settings (hor reload)

	* change a part of your insfrastructure

	* more dynamic than ansible, puppet...

	* reuse specific variables from pi to convert its to openmetric labels

-----------------------------------------------------------------------

# VICTORIA METRICS : Concepts & Definitions

<br>

Remote Write

	* long life storage (ex : VM, thanos for prometheus)

	* high retention or/and high numbre of metrics
	
-----------------------------------------------------------------------

# VICTORIA METRICS : Concepts & Definitions

<br>

Deduplication

	* avoid to store duplicated metrics 

  * for example : ha with 2 prometheus


-----------------------------------------------------------------------

# VICTORIA METRICS : Concepts & Definitions

<br>

Downsampling

	* automatic sampling of your metrics

	* dicrease the volume 

	* good for long retention

	* allow us to analyze on large time range


-----------------------------------------------------------------------

# VICTORIA METRICS : Concepts & Definitions

<br>

More on this page : 
		https://docs.victoriametrics.com/keyConcepts.html
