%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 

-----------------------------------------------------------------------------------------------------------                                       

# Traefik - Installation & Configuration ??

<br>

0. Change proxy rules...

1. Create dns or nip.io or direct ip with specific route...

2. Create instance

3. install traefik

4. configure static configuration

5. configure dynamix configuration

-----------------------------------------------------------------------------------------------------------                                       

# Traefik - Installation & Configuration ??

<br>

Configuration - entrypoints http/https

```
[entryPoints]
  [entryPoints.http]
    address = ":80"
    [entryPoints.http.http.redirections]
      [entryPoints.http.http.redirections.entryPoint]
        to = "https"
        scheme = "https"
    [entryPoints.http.http2]
      maxConcurrentStreams = 250
  [entryPoints.https]
    address = ":443"
    [entryPoints.https.http2]
      maxConcurrentStreams = 250
```

-----------------------------------------------------------------------------------------------------------                                       

# Traefik - Installation & Configuration ??

<br>

Configuration - logs

```
[certificatesResolvers.test.acme]
  email = "x@moi.fr"
  caServer = "https://acme-staging-v02.api.letsencrypt.org/directory"
  storage = "acme.json"
  keyType = "EC384"
  [certificatesResolvers.test.acme.httpChallenge]
    entryPoint = "http"
```
Note : caServer = "https://acme-v02.api.letsencrypt.org/directory"

-----------------------------------------------------------------------------------------------------------                                       

# Traefik - Installation & Configuration ??

<br>

Dynamic config - router

```
[http]
  [http.routers]
    [http.routers.api]
      rule = "PathPrefix(`/api`, `/dashboard`)"
      entryPoints = ["api"]
      service = "api@internal"
      middlewares = ["auth"]
    [http.routers.myservice]
      rule = "Host(`test-195-15-198-66.nip.io`)"
      entryPoints = ["http","https"]
      service = "myservice@file"
      middlewares = ["auth"]
      [http.routers.myservice.tls]
        certResolver = "test"
```

-----------------------------------------------------------------------------------------------------------                                       

# Traefik - Installation & Configuration ??

<br>

Dynamic config - middlewares

```
  [http.middlewares]
    [http.middlewares.auth.basicAuth]
      users = [
      "monnomestpersonne:$apr1$rnqfuhkt$u7ubv2bBKQYNscWTw2xy./"
      ]
    [http.middlewares.to_https.redirectScheme]
      scheme = "https"
      permanent = true
    [http.middlewares.secure_headers.headers]
      framedeny = true
      browserxssfilter = true
      contentTypeNosniff = true
      stsIncludeSubdomains = true
      stsPreload = true
      stsSeconds = 31536000
      forceStsHeader = true
      referrerPolicy = "same-origin"
```

-----------------------------------------------------------------------------------------------------------                                       

# Traefik - Installation & Configuration ??

<br>

Dynamic config - services

```
[http.services]
  [http.services.myservice.loadBalancer]
    [[http.services.myservice.loadBalancer.servers]]
      url = "http://grafana.service.consul:3000/"
```

-----------------------------------------------------------------------------------------------------------                                       

# Traefik - Installation & Configuration ??

<br>

Dynamic config - conf tls

```
[tls]
  [tls.options]
    [tls.options.default]
      minVersion = "VersionTLS13"
      sniStrict = true
      cipherSuites = [
        "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384",
        "TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305",
      ]
      curvePreferences = ["CurveP521","CurveP384"]
```

Note : https://ssl-config.mozilla.org/
