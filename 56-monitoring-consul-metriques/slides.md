%title: Infra Cloud Infomaniak
%author: xavki


 ██████╗ ██████╗ ███╗   ██╗███████╗██╗   ██╗██╗         
██╔════╝██╔═══██╗████╗  ██║██╔════╝██║   ██║██║         
██║     ██║   ██║██╔██╗ ██║███████╗██║   ██║██║         
██║     ██║   ██║██║╚██╗██║╚════██║██║   ██║██║         
╚██████╗╚██████╔╝██║ ╚████║███████║╚██████╔╝███████╗    
 ╚═════╝ ╚═════╝ ╚═╝  ╚═══╝╚══════╝ ╚═════╝ ╚══════╝    


-----------------------------------------------------------------------------------------------------------                                       

# Activate Consul metrics : self metrics and exporter

<br>

Fix : 
  
  * Loki / Log quotes

  * check_target

  * traefik dashboard > tcp

<br>

consul exporter = metrics for services / nodes

telemetry of cluster = metrics from master nodes

-----------------------------------------------------------------------------------------------------------                                       

# Activate Consul metrics : self metrics and exporter

<br>

Telemetry

```
    "telemetry": {
        "prometheus_retention_time": "48h",
        "disable_hostname": true
    }
```

-----------------------------------------------------------------------------------------------------------                                       

# Activate Consul metrics : self metrics and exporter

<br>

Add the scrape in vmagent

```
- job_name: consul
  consul_sd_configs:
  - server: 'consul.service.xavki.consul:8500'
    services:
    - 'consul'
  metrics_path: '/v1/agent/metrics'
  scheme: http
  param: 
    format: ["prometheus"]
  relabel_configs:
  - source_labels: ['__meta_consul_address']
    regex:         '(.*)'
    target_label:  '__address__'
    replacement:   '${1}:8500'
  - source_labels: [__meta_consul_node]
    target_label: instance
```

-----------------------------------------------------------------------------------------------------------                                       

# Activate Consul metrics : self metrics and exporter

<br>

Add consul exporter role

```
consul_exporter_version: 0.11.0
consul_exporter_binary_path: /usr/local/bin
consul_exporter_address: "consul.service.consul:8500"
```

-----------------------------------------------------------------------------------------------------------                                       

# Activate Consul metrics : self metrics and exporter

<br>

Add consul exporter role

```
- name: create group consul_exporter
  ansible.builtin.group:
    name: consul_exporter
    system: yes
    state: present
```

-----------------------------------------------------------------------------------------------------------                                       

# Activate Consul metrics : self metrics and exporter

<br>

Add consul exporter role

```
- name: create user consul_exporter
  ansible.builtin.user:
    name: consul_exporter
    system: yes
    shell: /sbin/nologin
    state: present
```

-----------------------------------------------------------------------------------------------------------                                       

# Activate Consul metrics : self metrics and exporter

<br>

Add consul exporter role

```
- name: check if consul_exporter exists
  ansible.builtin.stat:
    path: "{{ consul_exporter_binary_path }}/consul_exporter"
  register: __consul_exporter_exists
```

-----------------------------------------------------------------------------------------------------------                                       

# Activate Consul metrics : self metrics and exporter

<br>

Add consul exporter role

```
- name: if consul_exporter exists get version
  ansible.builtin.shell: "cat /etc/systemd/system/consul_exporter.service | grep Version | sed s/'.*Version '//g"
  register: __consul_exporter_get_version
  when: __consul_exporter_exists.stat.exists == true
  changed_when: false
```

-----------------------------------------------------------------------------------------------------------                                       

# Activate Consul metrics : self metrics and exporter

<br>

Add consul exporter role

```
- name: download consul_exporter
  ansible.builtin.unarchive:
    src: https://github.com/prometheus/consul_exporter/releases/download/v{{ consul_exporter_version }}/consul_exporter-{{ consul_exporter_version }}.linux-amd64.tar.gz        
    dest: "{{ consul_exporter_binary_path }}"
    remote_src: yes
    mode: 0750
    owner: consul_exporter
    group: consul_exporter
    extra_opts:
    - consul_exporter-0.11.0.linux-amd64/consul_exporter
    - --strip-components=1
  when: __consul_exporter_exists.stat.exists == False or not __consul_exporter_get_version.stdout == consul_exporter_version
```

-----------------------------------------------------------------------------------------------------------                                       

# Activate Consul metrics : self metrics and exporter

<br>

Add consul exporter role

```
- name: create service consul_exporter for agent
  ansible.builtin.template:
    src: "consul_exporter.service.j2"
    dest: "/etc/systemd/system/consul_exporter.service"
    mode: 0750
    owner: "consul_exporter"
    group: "consul_exporter"
  notify: reload_daemon_and_restart_consul_exporter
```

-----------------------------------------------------------------------------------------------------------                                       

# Activate Consul metrics : self metrics and exporter

<br>

Add consul exporter role

```
- meta: flush_handlers

- name: start consul_exporter
  ansible.builtin.service:
    name: consul_exporter
    state: started
    enabled: yes
```

-----------------------------------------------------------------------------------------------------------                                       

# Activate Consul metrics : self metrics and exporter

<br>

Add consul exporter role

```
- name: reload_daemon_and_restart_consul_exporter
  systemd:
    name: consul_exporter
    state: restarted
    daemon_reload: yes
    enabled: yes
```

-----------------------------------------------------------------------------------------------------------                                       

# Activate Consul metrics : self metrics and exporter

<br>

Add consul exporter role

```
[Unit]
Description=Consul Exporter Version {{ consul_exporter_version }}
Documentation=https://github.com/prometheus/consul_exporter/releases
After=network-online.target
Wants=network-online.target

[Service]
Type=simple
User=consul_exporter
Group=consul_exporter
ExecStart={{ consul_exporter_binary_path }}/consul_exporter \
    --consul.server={{ consul_exporter_address }} \
    --consul.timeout=30s

ExecReload=/bin/kill -HUP $MAINPID
KillSignal=SIGINT
TimeoutStopSec=5
Restart=on-failure
SyslogIdentifier=consul_exporter

[Install]
WantedBy=multi-user.target
```

-----------------------------------------------------------------------------------------------------------                                       

# Activate Consul metrics : self metrics and exporter

<br>


Add the consul exporter service

```
- {
  name: "consul_exporter",
  type: "http",
  check_target: "http://127.0.0.1:9107",
  interval: "20s",
  port: 9107
  }
```

-----------------------------------------------------------------------------------------------------------                                       

# Activate Consul metrics : self metrics and exporter

<br>

Add the scrape in vmagent

```
- job_name: consul_exporter
  consul_sd_configs:
  - server: 'consul.service.xavki.consul:8500'
    services:
    - 'consul_exporter'
  relabel_configs:
  - source_labels: ['__meta_consul_address']
    regex:         '(.*)'
    target_label:  '__address__'
    replacement:   '${1}:9107'
  - source_labels: [__meta_consul_node]
    target_label: instance
```

