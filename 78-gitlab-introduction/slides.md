%title: Infra Cloud Infomaniak
%author: xavki


 ██████╗ ██╗████████╗██╗      █████╗ ██████╗ 
██╔════╝ ██║╚══██╔══╝██║     ██╔══██╗██╔══██╗
██║  ███╗██║   ██║   ██║     ███████║██████╔╝
██║   ██║██║   ██║   ██║     ██╔══██║██╔══██╗
╚██████╔╝██║   ██║   ███████╗██║  ██║██████╔╝
 ╚═════╝ ╚═╝   ╚═╝   ╚══════╝╚═╝  ╚═╝╚═════╝ 



-----------------------------------------------------------------------------------------------------------

# Gitlab : Introduction

<br>

What is it ? devops lifecycle

  * plan

  * code

  * build

  * test

  * release

  * deploy

  * operate

  * monitor

-----------------------------------------------------------------------------------------------------------

# Gitlab : Introduction

<br>

What is it ? standard devops tools

  * git repository with GUI

  * gitlab runner : continuous integration & continuous deployment

  * docker registry

  * terraform states & modules

  * issues & tasks : boards, roadmaps

  * wiki & web pages

  * apps security : scans...

  * provide an API


-----------------------------------------------------------------------------------------------------------

# Gitlab : Introduction

<br>

Editions :

  * CE : community edition

  * EE : enterprise edition

premium = 29€ per user / month

-----------------------------------------------------------------------------------------------------------

# Gitlab : Introduction

<br>

Installation

  https://docs.gitlab.com/ee/install/install_methods.html